# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
def post_list(request):
    return render(request, 'game/gamehome.html', {})

def post_new(request):
    return render(request, 'game/late.html', {})

def post_list3(request):
    return render(request, 'game/omen.html', {})

def post_list4(request):
    return render(request, 'game/right.html', {})

def post_list5(request):
    return render(request, 'game/end.html', {})

def post_list6(request):
    return render(request, 'game/next.html', {})

def post_list7(request):
    return render(request, 'game/zero.html', {})

def post_list8(request):
    return render(request, 'game/ohmygod.html', {})

def post_list9(request):
    return render(request, 'game/lorenzo.html', {})
