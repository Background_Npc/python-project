from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$',views.post_list, name='post_list'),
	url(r'^late/$',views.post_new, name='post_new'),
	url(r'^omen/$',views.post_list3, name='post_list3'),
	url(r'^right/$',views.post_list4, name='post_list4'),
	url(r'^end/$',views.post_list5, name='post_list5'),
	url(r'^next/$',views.post_list6, name='post_list6'),
	url(r'^zero/$',views.post_list7, name='post_list7'),
	url(r'^orcs/$',views.post_list8, name='post_list8'),
	url(r'^lorenzo/$',views.post_list9, name='post_list9'),
]
