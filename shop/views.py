from django.shortcuts import render, get_object_or_404
from .models import Category, Product
from cart.forms import CartAddProductForm

def product_list(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)

    context = {
        'category': category,
        'categories': categories,
        'products': products
    	}
    return render(request, 'npcshop/product/list.html', context)


def product_detail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    cart_product_form = CartAddProductForm()
    context = {
        'product': product,
        'cart_product_form': cart_product_form
    	}
    return render(request, 'npcshop/product/detail.html', context)

def post_list(request):
    return render(request, 'game/gamehome.html', {})

def post_new(request):
    return render(request, 'game/late.html', {})

def post_list3(request):
    return render(request, 'game/omen.html', {})

def post_list4(request):
    return render(request, 'game/right.html', {})

def post_list5(request):
    return render(request, 'game/end.html', {})

def post_list6(request):
    return render(request, 'game/next.html', {})

def post_list7(request):
    return render(request, 'game/zero.html', {})

def post_list8(request):
    return render(request, 'game/orcs.html', {})

def post_list9(request):
    return render(request, 'game/lorenzo.html', {})
