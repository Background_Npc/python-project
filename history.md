history for python project (website)
-python 3.6.1
-flake8
-version control: git
-django 1.11
-code editor : atom
-Pillow 3.0.0
-Boostrap4
-Jquery

April 27,2018
-updated python to 3.6.1
-installed django 1.11
-updated pip to 10.0.1

commit: basefoundation
commit: add superuser
commit: tag version 1.0

May 4,2018
commit:website.

June 6, 2018
-installed Pillow
-updated models.py
-updated views.py
-updated urls.py

June 7, 2018
-updated website
-updated database
-new css
-added several html
-added bootstrap and jquery.
-added shoppingcart system
-added game website
-added diceroller.py


